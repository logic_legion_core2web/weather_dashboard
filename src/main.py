#   most recently updated code

from tkinter import *
import tkinter as tk
from geopy.geocoders import Nominatim
from tkinter import ttk, messagebox
from timezonefinder import TimezoneFinder 
from datetime import datetime
from datetime import *
from PIL import ImageTk, Image
from geopy.geocoders import Photon
import requests
import pytz

root = Tk()
root.title("Weather Dashboard")
root.geometry("890x470+300+200")   # ("890x470+300+200")
root.configure(bg = "#57adff")
root.resizable(False,False)

secondimage = None
thirdimage = None
fourthimage = None
fifthimage = None
siximage = None
sevenimage = None

def getWeather():

    global secondimage, thirdimage, fourthimage, fifthimage, siximage, sevenimage

    try:
        city = textfield.get()
    
        #geolocator = Nominatim(user_agent = "geoapiExercises") #===> original
        geolocator = Photon(user_agent="geoapiExercises")
        location = geolocator.geocode(city)
        obj = TimezoneFinder()

        result = obj.timezone_at(lng = location.longitude,lat = location.latitude)
   
        timezone.config(text = result)   
        long_lat.config(text=f"{round(location.longitude,2)}°E, {round(location.latitude,2)}°N")

        home = pytz.timezone(result)
        local_time = datetime.now(home)
        current_time = local_time.strftime("%I:%M %p")      # %I Hour (12-hour clock) as a zero-padded decimal number. 01, 02, ..., 12
                                                            # %M Minute as a zero-padded decimal number. 00, 01, ..., 59
                                                            # %p Locale’s AM or PM.
        clock.config(text = current_time)

        #weather

        #api = "https://api.openweathermap.org/data/2.5/onecall?lat="+str(location.latitude)+"&lon="+"77.1025"+"&units=metric&exclude=hourly&appid=54d9b060e60098607363e3342edadf5e"

        #api = "https://api.openweathermap.org/data/2.5/onecall?lat="+"28.7041"+"&lon="+"77.1025"+"&units=metric&exclude=hourly,daily&appid=c14cb8ed5d1ad2986ae41bd6e87bec56"

        api = "https://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=c14cb8ed5d1ad2986ae41bd6e87bec56"

        json_data = requests.get(api).json()

        #temp = json_data["current"]['temp']
        temp = int(json_data['main']['temp']-273.15)
    
        #humidity = json_data['current']['humidity']
        humidity = json_data['main']['humidity']

        #pressure = json_data['current']['pressure']
        pressure = json_data['main']['pressure']

        #wind = json_data['current']['wind_speed']
        wind = json_data['wind']['speed']

        #description = json_data['current']['weather'][0]['description']
        description = json_data['weather'][0]['description']
 
        t.config(text=(temp,"°C"))
        h.config(text=(humidity,"%"))
        p.config(text = (pressure,"hPa"))
        w.config(text = (wind,"m/s"))
        d.config(text =(description))

        #first cell
        firstdayimage = json_data['weather'][0]['icon']

        img = (Image.open(f"icons/{firstdayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo1 = ImageTk.PhotoImage(resized_image)
        firstimage.config(image=photo1)
        firstimage.image = photo1 


        #photo1 = ImageTk.PhotoImage(file=f"icons/{firstdayimage}@2x.png")
        #firstimage.config(image=photo1)
        #firstimage.image = photo1 

        tempday1 = int(json_data['main']['temp']-273.15)
        tempnight1 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day1temp.config(text=f"Day:{tempday1}°C\n T_Min:{tempnight1}°C")

        #second cell

        seconddayimage = json_data['weather'][0]['icon']

        img = (Image.open(f"icons/{seconddayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo2 = ImageTk.PhotoImage(resized_image)
    
        secondimage.config(image=photo2)
        secondimage.image =photo2
        

        tempday2 = int(json_data['main']['temp']-273.15)
        tempnight2 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day2temp.config(text=f"Day:{tempday2}\n T_Min:{tempnight2}")

        #third cell    

        thirddayimage = json_data['weather'][0]['icon']
    
        img = (Image.open(f"icons/{thirddayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo3 = ImageTk.PhotoImage(resized_image)
        thirdimage.config(image=photo3)
        thirdimage.image =photo3

        tempday3 = int(json_data['main']['temp']-273.15)
        tempnight3 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day3temp.config(text=f"Day:{tempday3}\n T_Min:{tempnight3}")

        #fourth cell

        fourthdayimage = json_data['weather'][0]['icon']
    
        img = (Image.open(f"icons/{fourthdayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo4 = ImageTk.PhotoImage(resized_image)
        fourthimage.config(image=photo4)
        fourthimage.image =photo4

        tempday4 = int(json_data['main']['temp']-273.15)
        tempnight4 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day4temp.config(text=f"Day:{tempday4}\n T_Min:{tempnight4}")

        # fifth cell

        fifthdayimage = json_data['weather'][0]['icon']
    
        img = (Image.open(f"icons/{fifthdayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo5 = ImageTk.PhotoImage(resized_image)
        fifthimage.config(image=photo5)
        fifthimage.image=photo5

        tempday5 = int(json_data['main']['temp']-273.15)
        tempnight5 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day5temp.config(text=f"Day:{tempday5}\n T_Min:{tempnight5}")

        # sixth cell

        sixthdayimage = json_data['weather'][0]['icon']
        img = (Image.open(f"icons/{sixthdayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo6 = ImageTk.PhotoImage(resized_image)
        siximage.config(image=photo6)
        siximage.image = photo6

        tempday6 = int(json_data['main']['temp']-273.15)
        tempnight6 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day6temp.config(text=f"Day:{tempday6}\n T_Min:{tempnight6}")

        # seventh cell

        seventhdayimage = json_data['weather'][0]['icon']
    
        img = (Image.open(f"icons/{seventhdayimage}@2x.png"))
        resized_image = img.resize((50,50))
        photo7 = ImageTk.PhotoImage(resized_image)
        sevenimage.config(image=photo7)
        sevenimage.image=photo7
 
        tempday7 = int(json_data['main']['temp']-273.15)
        tempnight7 = int(json_data['main']['temp_min']-273.15)      # should be chnaged

        day7temp.config(text=f"Day:{tempday7}\n T_Min:{tempnight7}")

        # days

        first = datetime.now()
        day1.config(text=first.strftime("%A"))

        second = first + timedelta(days=1)
        day2.config(text=second.strftime("%A"))

        third = first + timedelta(days=2)
        day3.config(text=third.strftime("%A"))

        fourth = first + timedelta(days=3)
        day4.config(text=fourth.strftime("%A"))

        fifth = first + timedelta(days=4)
        day5.config(text=fifth.strftime("%A"))

        sixth = first + timedelta(days=5)
        day6.config(text=sixth.strftime("%A"))

        seventh = first + timedelta(days=6)
        day7.config(text=seventh.strftime("%A"))

    except Exception as e:
        messagebox.showerror("Weather Dashboard","Invalid City!!!!")

##icon

image_icon = PhotoImage(file="Images/logo.png")   ###logos
root.iconphoto(False,image_icon)   #Afer this image will be added to display

# 1 st rectangle box ==> contains tempearture,Humidity, Pressure, Wind Speed, Description, etc
Round_box = PhotoImage(file = "Images/Rounded Rectangle 1.png")
Label(root,image = Round_box,bg = "#57adff").place(x = 45,y = 110)

#Label

label1 = Label(root,text = "Temperature",font = ("Helvetica",11),fg = "white",bg = "#203243")
label1.place(x = 50,y = 120)

label2 = Label(root,text = "Humidity",font = ("Helvetica",11),fg = "white",bg = "#203243")
label2.place(x = 50,y = 140)

label3 = Label(root,text = "Pressure",font = ("Helvetica",11),fg = "white",bg = "#203243")
label3.place(x = 50,y = 160)

label4 = Label(root,text = "Wind Speed",font = ("Helvetica",11),fg = "white",bg = "#203243")
label4.place(x = 50,y = 180)

label5 = Label(root,text = "Description",font = ("Helvetica",11),fg = "white",bg = "#203243")
label5.place(x = 50,y = 200)
#after that labels will get created


# Search box

Search_image = PhotoImage(file="Images/Rounded Rectangle 3.png")
myimage = Label(image = Search_image,bg = "#57adff")
myimage.place(x = 270, y = 120) #After that we will see search box on the screen


weat_image = PhotoImage(file="Images/Layer 7.png")
weatherimage = Label(root,image=weat_image,bg="#203243")
weatherimage.place(x = 290, y = 127)

textfield = tk.Entry(root,justify="center",width=15,font=("poppins",22,"bold"),bg="#203242",border=0,fg="white")    # original width = 25 was
textfield.place(x = 370,y = 130) # original (x=370,y=130)
textfield.focus()


search_icon = PhotoImage(file="Images/Layer 6.png")
myimage_icon = Button(image=search_icon,borderwidth=0,cursor="hand2",bg = "#203243",command=getWeather)
myimage_icon.place(x = 645,y = 125)

#bottom box
frame = Frame(root,width=900,height=180,bg = "#212120") # width was 900
frame.pack(side=BOTTOM)


#bottom boxes
firstbox = PhotoImage(file="Images/Rounded Rectangle 2.png")
secondbox = PhotoImage(file="Images/Rounded Rectangle 2 copy.png")

Label(frame,image = firstbox,bg = "#212120").place(x = 30,y = 20)   # originally x=30, y = 20
Label(frame,image = secondbox,bg = "#212120").place(x = 300,y = 30) # originally x=300, y = 30
Label(frame,image = secondbox,bg = "#212120").place(x = 400,y = 30) # originally x=400, y = 30
Label(frame,image = secondbox,bg = "#212120").place(x = 500,y = 30) # originally x=500, y = 30
Label(frame,image = secondbox,bg = "#212120").place(x = 600,y = 30) # originally x=600, y = 30
Label(frame,image = secondbox,bg = "#212120").place(x = 700,y = 30) # originally x=700, y = 30
Label(frame,image = secondbox,bg = "#212120").place(x = 800,y = 30) # originally x=800, y = 30


# clock (here we will place time)
clock = Label(root,font = ("Helvetica",30,'bold'),fg="white",bg = "#57adff")
clock.place(x = 30,y = 20)

# timezone
timezone = Label(root,font = ("Helvetica",17, 'bold'),fg="white",bg = "#57adff")
timezone.place(x = 650,y = 20)  # x = 700,y = 20

# logitude
long_lat = Label(root,font = ("Helvetica",20, 'bold'),fg="white",bg = "#57adff")
long_lat.place(x = 650,y = 50)  # x = 700,y = 50


#thpwd

t = Label(root,font = ("Helvetica",11),fg="white",bg="#203243")
t.place(x = 150,y = 120)

h = Label(root,font = ("Helvetica",11),fg="white",bg="#203243")
h.place(x = 150,y = 140)

p = Label(root,font = ("Helvetica",11),fg="white",bg="#203243")
p.place(x = 150,y = 160)

w = Label(root,font = ("Helvetica",11),fg="white",bg="#203243")
w.place(x = 150,y = 180)

d = Label(root,font = ("Helvetica",11),fg="white",bg="#203243")
d.place(x = 150,y = 200)

# first cell
firstframe = Frame(root, width=230,height=132,bg="#282829")
firstframe.place(x=35,y=315)    # original x = 35, y = 315

day1 = Label(firstframe, font="arial 20",bg="#282829",fg="#fff")
day1.place(x=60,y=5)        # original x = 100

firstimage = Label(firstframe, bg="#282829")
firstimage.place(x=1,y=40,height=80,width=100)

day1temp = Label(firstframe,bg="#282829",fg="#57adff",font="arial 15 bold")
day1temp.place(x=100,y=50)

# second cell
secondframe = Frame(root, width=70,height=115,bg="#282829")
secondframe.place(x=305,y=324) 

day2 = Label(secondframe,bg="#282829",fg="#fff")
day2.place(x=6,y=5)        

secondimage = Label(secondframe, bg="#282829")
secondimage.place(x=7,y=20)

day2temp = Label(secondframe,bg="#282829",fg="#fff")
day2temp.place(x=2,y=70)

# third cell
thirdframe = Frame(root, width=70,height=115,bg="#282829")
thirdframe.place(x=405,y=324) 

day3 = Label(thirdframe,bg="#282829",fg="#fff")
day3.place(x=6,y=5)  

thirdimage = Label(thirdframe, bg="#282829")
thirdimage.place(x=7,y=20)

day3temp = Label(thirdframe,bg="#282829",fg="#fff")
day3temp.place(x=2,y=70)

# fourth cell
fourthframe = Frame(root, width=70,height=115,bg="#282829")
fourthframe.place(x=505,y=324) 

day4 = Label(fourthframe,bg="#282829",fg="#fff")
day4.place(x=6,y=5)  

fourthimage = Label(fourthframe, bg="#282829")
fourthimage.place(x=7,y=20)

day4temp = Label(fourthframe,bg="#282829",fg="#fff")
day4temp.place(x=2,y=70)

# fifth cell
fifthframe = Frame(root, width=70,height=115,bg="#282829")
fifthframe.place(x=605,y=324) 

day5 = Label(fifthframe, bg="#282829",fg="#fff")
day5.place(x=6,y=5)  

fifthimage = Label(fifthframe, bg="#282829")
fifthimage.place(x=7,y=20)

day5temp = Label(fifthframe,bg="#282829",fg="#fff")
day5temp.place(x=2,y=70)

# sixth cell
sixframe = Frame(root, width=70,height=115,bg="#282829")
sixframe.place(x=705,y=324) 

day6 = Label(sixframe, bg="#282829",fg="#fff")
day6.place(x=6,y=5)  

siximage = Label(sixframe, bg="#282829")
siximage.place(x=7,y=20)

day6temp = Label(sixframe,bg="#282829",fg="#fff")
day6temp.place(x=2,y=70)

# seventh cell
sevenframe = Frame(root, width=70,height=115,bg="#282829")
sevenframe.place(x=805,y=324) 

day7 = Label(sevenframe, bg="#282829",fg="#fff")
day7.place(x=6,y=5)        

sevenimage = Label(sevenframe, bg="#282829")
sevenimage.place(x=7,y=20)

day7temp = Label(sevenframe,bg="#282829",fg="#fff")
day7temp.place(x=2,y=70)

root.mainloop()